from tests.base_test import BaseTest


class BasePage(BaseTest):
    """
        common screen locators
    """
    def __init__(self, driver):
        super().__init__(driver)
