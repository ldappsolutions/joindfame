from tests.base_test import BaseTest
import time


class LoginPage(BaseTest):
    """
    login screen locators
    """
    def home(self):
        login = self.driver.find_element_by_xpath("//*[@text='log in outline  LOGIN']")
        login.click()
        permission = self.driver.find_element_by_xpath("//*[@text='Allow']")
        permission.click()
        time.sleep(5)
        country = self.driver.find_element_by_xpath("//*[@text='caret down outline']").click()
        time.sleep(5)

    def select_country_code(self, code):
        # This method is to select country code on login screen
        drop_down = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout"
                                                      "/android.widget.LinearLayout"
                                                      "/android.widget.FrameLayout/android.widget.FrameLayout"
                                                      "/android.webkit.WebView/android.webkit.WebView"
                                                      "/android.view.View/android.view.View/android.view.View[2]"
                                                      "/android.app.Dialog/android.view.View/android.view.View[1]"
                                                      "/android.view.View[2]/android.view.View/android.widget.EditText")
        drop_down.click()
        drop_down.send_keys(code)
        options = self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout"
                                                    "/android.widget.FrameLayout/android.widget.FrameLayout"
                                                    "/android.webkit.WebView/android.webkit.WebView/android.view.View"
                                                    "/android.view.View/android.view.View[2]/android.app.Dialog"
                                                    "/android.view.View/android.view.View[2]/android.view.View"
                                                    "/android.view.View[1]/android.view.View")
        options.click()

    def enter_mobile_number(self, mobile_number):
        # This method is to enter mobile number on login screen
        text_field = self.driver.find_element_by_class_name('android.widget.EditText')
        text_field.send_keys(mobile_number)

    def enter_password(self, password):
        # This method is to enter password on login screen
        text_field = self.driver.find_element_by_xpath("//*[@text='Password']")
        text_field.send_keys(password)

    def click_login_button(self):
        # This method is to click on LOGIN button
        login_button = self.driver.find_element_by_xpath("//*[@text='log in   LOGIN']")
        login_button.click()

    def assert_login_successful(self):
        # This method to check login is successful
        login_button = self.driver.find_element_by_xpath("//*[@text='log in   LOGIN']")
        self.assertTrue(login_button.is_enabled(), "Login is not successfull")
