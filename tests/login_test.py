import unittest
from tests.base_test import BaseTest
from page_objects.login_page import LoginPage
import time
import os
import HtmlTestRunner


class LoginTest(LoginPage):

    def test_login(self):
        LoginPage.home(self)
        LoginPage.select_country_code(self, "91")
        LoginPage.enter_mobile_number(self, '9842504019')
        LoginPage.enter_password(self, 'joindfame')
        LoginPage.click_login_button(self)
        LoginPage.assert_login_successful(self)

    def test_invalid_login(self):
        LoginPage.home(self)
        LoginPage.select_country_code(self, "91")
        LoginPage.enter_mobile_number(self, '984250019')
        LoginPage.enter_password(self, 'joindfame')
        LoginPage.click_login_button(self)
        LoginPage.assert_login_successful(self)


if __name__ == '__main__':
    test_report_directory = "../reports/"
    current_directory = os.path.dirname(__file__)
    destination_directory = os.path.join(current_directory, test_report_directory)
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output=destination_directory))
