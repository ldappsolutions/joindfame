from appium import webdriver
import unittest
import os
from datetime import datetime


class BaseTest(unittest.TestCase):
    dc = {}
    driver = None

    @classmethod
    def setUpClass(cls):
        """"This method before all tests and will open app in the device specified"""
        resources_directory = "../resources/joindfame.apk"
        current_directory = os.path.dirname(__file__)
        destination_directory = os.path.join(current_directory, resources_directory)
        cls.dc['app'] = destination_directory
        cls.dc['appPackage'] = 'com.jdf.joindfame'
        cls.dc['appActivity'] = 'com.jdf.joindfame.MainActivity'
        cls.dc['platformName'] = 'Android'
        cls.dc['platformVersion'] = '10'
        cls.dc['deviceName'] = '3201ca4ecc0a166b'
        cls.driver = webdriver.Remote('http://localhost:4723/wd/hub', cls.dc)
        cls.driver.implicitly_wait(15)

    def setUp(self):
        """This method will execute before each test"""
        pass

    def tearDown(self):
        """This method will execute each test and will take screen shot after each test"""
        self.take_screen_shot()

    def take_screen_shot(self):
        """Method to take screen shot and store in a directory"""
        file_name = self._testMethodName + datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + ".png"
        screen_shot_directory = "../screenshots/"
        relative_file_name = screen_shot_directory + file_name
        current_directory = os.path.dirname(__file__)
        destination_file = os.path.join(current_directory, relative_file_name)
        destination_directory = os.path.join(current_directory, screen_shot_directory)
        if not os.path.exists(destination_directory):
            os.makedirs(destination_directory)
        self.driver.save_screenshot(destination_file)

    @classmethod
    def tearDownClass(cls):
        """This method will execute after all tests. This method will quit the driver"""
        cls.driver.quit()


if __name__ == '__main__':
    unittest.main()
